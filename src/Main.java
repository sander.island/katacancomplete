import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        //canComplete("butl","beautiful");
        System.out.println(canComplete("butl","beautiful"));
    }

    // This method will return true if it can complete, but will also return true in some occasions when it isn't.
    public static boolean canComplete(String a, String b)
    {
        boolean canComplete = false;
        for (int i = 0; i < a.length(); i++)
        {
            b.contains(Character.toString(a.charAt(i)));
            if (!b.contains(Character.toString(a.charAt(i))))
            {
                canComplete = false;
                break;
            }
            else {
                canComplete = true;
            }
        }

        //System.out.println(a);
        //System.out.println(b);
        return canComplete;

    }

    // Martin's code
    private static boolean isComplete(String finalWord, String testStr){
        HashMap<Integer, Character> letterToIndex = new HashMap<>();
        HashMap<Integer, Character> controlMap = new HashMap<>();
        for (int i = 0; i < finalWord.length(); i++){
            char ch = finalWord.charAt(i);
            letterToIndex.put(i, ch);
        }
        int[] indexArr = new int[testStr.length()];
        for (int j = 0; j < testStr.length(); j++){
            char ch = testStr.charAt(j);
            for (Map.Entry<Integer, Character> entry : letterToIndex.entrySet()){
                if (Character.compare(ch, entry.getValue()) == 0){
                    indexArr[j] = entry.getKey();
                    letterToIndex.remove(entry.getKey());
                    break;
                }
            }
        }
        for (int k = 0; k < indexArr.length - 1; k++){
            if (indexArr[k] < indexArr[k+1]){
                continue;
            }
            else {
                return false;
            }
        }
        return true;
    }

}
